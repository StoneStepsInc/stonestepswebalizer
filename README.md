Source code and downloads for Stone Steps Webalizer have been moved 
to GitHub. See this Wiki page for details.

https://bitbucket.org/StoneStepsInc/stonestepswebalizer/wiki/Home
